# zadanie-3


##### 1. Connect to your VM
```
ssh student@sever1aX

Password: Dua1edu
```
> NOTE: 
- VMs are reachable from `phobos`
- replace `X` with your actual VM number e.g `server1a10` according to the following list:

```
server1a1  
server1a2   
server1a3  
server1a4   
server1a5   
server1a6   
server1a7  
server1a8   
server1a9   
server1a10  
server1a11
server1a12
server1a13
server1a14
server1a15
```


##### TASKS

1.  Vytvorte **3 nových užívateľov** podľa týchto kritérií:
```
	Uživateľ 1
	username: bob
	uid: 601
	shell: /bin/bash
	home dir: /home/bob
	comment: testuser1

	Uživateľ 2
	username: natasha
	uid: 602
	shell: /bin/bash
	home dir: /home/natasha
	comment: testuser2

	Uživateľ 3
	username: sam
	uid: 603
	shell: /bin/bash
	home dir: /home/sam
	comment: testuser3
```

2.  Novovytvoreným užívateľom nastavte defaultné heslo `Start123` a vynúťte zmenu hesla pri prvom prihlásení, platnosť užívateľského účtu nastavte do `31. 12.2024`.

3.  Vytvorte novú užívateľskú skupinu s názvom `testers` a priraďte jej gid `666`.

4.  Užívateľom `bob`, `natasha`, `sam` nastavte sekundárnu skupinu `testers`.

5.  Vytvorte prázdny priečinok `/share` a nastavte vlastníka `root` a skupinu `testers`.

6.  Prístupové práva pre `/share` upravte tak, aby vlastník a skupina mali plné (full) práva k tomuto priečinku.

7.  Zabezpečte, aby okrem vlastníka a skupiny nemal nikto iný prístup k `/share`. 

8.  Nastavte SGID pre priečinok `/share`.

9.  V domovskom adresári každého užívateľa vytvorte súbor `report.txt`, zmeňte vlastníka podľa konkrétneho užívateľa a skupinu zmeňte na `testers`.

10. Prístupové práva pre report.txt upravte nasledovne:
```
	vlastníci: 	môžu čítať a zapisovať do súboru
	skupina: 	môže len čítať
	ostatní: 	nemajú žiadne práva
```

11. Premenujte súbory `report.txt` analogicky v tvare `report_uzivatel.txt` a nakopírujte ich do `/share`.

12. Užívateľom `bob`, `natasha`, `sam` zadefinujte `sudo práva`, tak aby získali plné (root) práva.  

13. **Zdokumentovanie úloh**
- Vo `/var/tmp` si vytvorte nový priečinok s názvom `vase_wiw`, kde budete ukladať úlohy
- Zobrazte posledné 3 riadky súboru `/etc/passwd` a výsledok presmerujte do `uloha1.txt`
- Pomocou príkazu `id` zobrazte info o novovytvorených užívateľoch a výstupy uložte do `uloha2.txt`
- Pomocou príkazu `ls -ld` zobrazte info o priečinku `/share` a výstup presmerujte do `uloha3.txt`
- Vylistujte obsah priečinka `/share` (vrátane prístupových práv) a výstup presmerujte do `uloha4.txt`
- Pomocou príkazu `sudo -l -U username` vylistujte sudo práva pre užívateľov `bob`, `natasha`, `sam` a výstupy uložte do `uloha5.txt`
- Pomocou príkazu `chage -l username` zobrazte informácie o účtoch a výstupy presmerujte do `uloha6.txt`
- Ako root si príkazom `history` vypíšte zoznam naposledy použitých príkazov a výstup potom presmerujte do `uloha7.txt`
