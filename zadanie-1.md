# zadanie-1


##### 1. Connect to your VM
```
ssh student@sever1aX

Password: Dua1edu
```
> NOTE: 
- VMs are reachable from `phobos`
- replace `X` with your actual VM number e.g `server1a10` according to the following list:

```
server1a1  
server1a2   
server1a3  
server1a4   
server1a5   
server1a6   
server1a7  
server1a8   
server1a9   
server1a10  
server1a11
server1a12
server1a13
server1a14
```


##### TASKS

1.  Vytvorte nového užívateľa s názvom `tester` a nastavte mu heslo `Dua1edu`

2.  Prepnite sa na novovytvoreného usera, všetky nasledujúce úlohy vykonávajte pod týmto userom

3.  Vo svojom domovskom adresári si vytvorte pracovný adresár s názvom `exam` s podadresárom `results`

4.  V adresári `exam` vykonávajte všetky nasledovné úlohy

5.  Zobrazte posledný riadok súboru /etc/passwd a uložte výstup do súboru s názvom `result_1.txt`

6.  Presuňte `result_1.txt` do adresára `results`

7.  V pracovnom adresári vytvorte symlink s názvom `mytmp`, ktorý bude odkazovať na `/tmp` 

8.  Vytvorte súbor s názvom `mymail`, do ktorého uložíte svoju firemnú mailovú adresu

9. Nastavte prístupové práva k mymail, tak aby vlastník mal plné práva, skupina a ostatní žiadne

10. Zobrazte obsah súboru `mymail` na obrazovku a výstup presmerujte do `results/result_2.txt`

11. Vylistujte obsah aktuálneho adresára, vrátane prístupovych práv a výstup uložte do `results/result_3.txt`

12. V adresári “exam” vytvorte nasledovnú adresárovú štruktúru:
```
dualedu
├── 1.rok
│   ├── 1.polrok
│   └── 2.polrok
├── 2.rok
│   ├── 1.polrok
│   └── 2.polrok
└── 3.rok
    ├── 1.polrok
    └── 2.polrok
```

13. Vytvorte prázdny súbor `report.txt`, ktorý nakopírujete do koncových priečinkov

14. Nájdite všetky súbory v `/home/~/`, ktoré boli modifikované za posledný deň a výstup presmerujte do `results/result_4.txt`

15. Zistite počet riadkov súboru `/etc/passwd` a výsledok pridajte do súboru `results/result_1.txt`

16. Vystrihnite zo súboru `/etc/passwd` len užívateľské mená (1. stlpec), výsledok presmerujte do `results/result_5.txt`

17. Doinštalujte program `tree`

18. Pomoocu príkazu `tree` vyistujte obsah adresára DualEdu a výsledok presmerujte do súboru `results/result_6.txt`

19. Premenujte adresár `results` na `myresults`

20. Vytvorte 3 prázdne súbory a pomenujte ich  `testfile1.txt`, `testfile2.txt`, `testfile3.txt`

21. Zo súborov tesfile* vytvorte archive, komprimovaný pomocou gzip a pomenujte ho `archive.tgz`

22. `archive.tgz` umiestine to priečinka `myresults`